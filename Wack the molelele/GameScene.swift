//
//  GameScene.swift
//  Wack the molelele
//
//  Created by GDD Student on 11/7/16.
//  Copyright (c) 2016 GDD Student. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    var slots = [WhackSlot]()
    
    var youwin : SKLabelNode!
    var gameover : SKLabelNode!
    var gameScore: SKLabelNode!
    var score: Int = 0 {
        didSet {
            gameScore.text = "Score: \(score)"
        }
    }
    var gameLives: SKLabelNode!
    var lives: Int = 3 {
        didSet {
            gameLives.text = "Lives: \(lives)"
        }
    }

    
    var popupTime = 0.85
    var numRounds = 0
    override func didMoveToView(view: SKView) {
        let background = SKSpriteNode(imageNamed: "whackBackground")
        runAction(SKAction.playSoundFileNamed("background.mp3", waitForCompletion:false))
        
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .Replace
        background.zPosition = -1
        addChild(background)
        
        gameScore = SKLabelNode(fontNamed: "Chalkduster")
        gameScore.text = "Score: 0"
        gameScore.position = CGPoint(x: 10, y: 100)
        gameScore.horizontalAlignmentMode = .Left
        gameScore.fontSize = 48
        addChild(gameScore)
        
       gameLives = SKLabelNode(fontNamed: "Chalkduster")
        gameLives.text = "Lives: 3"
        gameLives.position = CGPoint(x: 500, y: 100)
        gameLives.horizontalAlignmentMode = .Left
        gameLives.fontSize = 48
        addChild(gameLives)
        
        for i in 0 ..< 5 { createSlotAt(CGPoint(x: 100 + (i * 170), y: 440)) }
        for i in 0 ..< 4 { createSlotAt(CGPoint(x: 180 + (i * 170), y: 350)) }
        for i in 0 ..< 5 { createSlotAt(CGPoint(x: 100 + (i * 170), y: 260)) }
        for i in 0 ..< 4 { createSlotAt(CGPoint(x: 180 + (i * 170), y: 170)) }
        
        RunAfterDelay(1) { [unowned self] in
            self.createEnemy()
        }
    }
    
    func createSlotAt(pos: CGPoint) {
        let slot = WhackSlot()
        slot.configureAtPosition(pos)
        addChild(slot)
        slots.append(slot)
    }
    
    func createEnemy() {
       /* numRounds += 1
        
        if numRounds >= 30 {
            for slot in slots {
                slot.hide()
            }
            
            let gameOver = SKSpriteNode(imageNamed: "gameOver")
            gameOver.position = CGPoint(x: 512, y: 384)
            gameOver.zPosition = 1
            addChild(gameOver)
            
            return
        } */
        if score == 20 {
            for slot in slots {
                slot.hide()
            }
            
            youwin = SKLabelNode(fontNamed: "Chalkduster")
            youwin.text = "YOU WIN"
            youwin.position = CGPoint(x: 512, y: 384)
            youwin.zPosition = 1
            addChild(youwin)
            
            return
        }
        if lives <= 0 {
            
            for slot in slots {
                slot.hide()
            }
            
            gameover = SKLabelNode(fontNamed: "Chalkduster")
            gameover.text = "YOU LOSE"
            gameover.position = CGPoint(x: 512, y: 384)
            gameover.zPosition = 1
            addChild(gameover)
            
            return
        }
        
        
        
        
        
        popupTime *= 0.991
        
        slots = GKRandomSource.sharedRandom().arrayByShufflingObjectsInArray(slots) as! [WhackSlot]
        slots[0].show(hideTime: popupTime)
        
        if RandomInt(min: 0, max: 12) > 4 { slots[1].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 8 {    slots[2].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 10 { slots[3].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 11 { slots[4].show(hideTime: popupTime)    }
        
        let minDelay = popupTime / 2.0
        let maxDelay = popupTime * 2.0
        
        RunAfterDelay(RandomDouble(min: minDelay, max: maxDelay)) { [unowned self] in
            self.createEnemy()
        }
    }
    
       override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.locationInNode(self)
            let nodes = nodesAtPoint(location)
            
            for node in nodes {
                if node.name == "charFriend" {
                    // they shouldn't have whacked this penguin
                    let whackSlot = node.parent!.parent as! WhackSlot
                    if !whackSlot.visible { continue }
                    if whackSlot.isHit { continue }
                    
                    whackSlot.charNode.xScale = 0.85
                    whackSlot.charNode.yScale = 0.85
                    
                    whackSlot.hit()
                    score += 1
                    
                    
                    
                    runAction(SKAction.playSoundFileNamed("boing.wav", waitForCompletion:false))
                    break
                }
                else if node.name == "hole"{
                
                    if lives <= 0{
                        return
                    }
                    NSLog("@touched hole!", "")
                    lives -= 1
                    break
                }
            }
        }
        
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
