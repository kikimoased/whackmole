//
//  WhackSlot.swift
//  Wack the molelele
//
//  Created by GDD Student on 11/7/16.
//  Copyright © 2016 GDD Student. All rights reserved.
//

import SpriteKit
import UIKit

class WhackSlot: SKNode {
    var charNode: SKSpriteNode!
    
    var visible = false
    var isHit = false
    
    func configureAtPosition(pos: CGPoint) {
        position = pos
        
        //let sprite = SKSpriteNode(imageNamed: "whackHole")
        //addChild(sprite)
        
        let holenode = SKCropNode()
        charNode = SKSpriteNode(imageNamed: "whackHole")
        holenode.name = "hole"
        holenode.addChild(charNode)
        addChild(holenode)
        
        let cropNode = SKCropNode()
        cropNode.position = CGPoint(x: 0, y: 15)
        cropNode.zPosition = 1
        cropNode.maskNode = SKSpriteNode(imageNamed: "whackMask")
        
        charNode = SKSpriteNode(imageNamed: "penguinGood")
        charNode.position = CGPoint(x: 0, y: -90)
        charNode.name = "character"
        cropNode.addChild(charNode)
        
        addChild(cropNode)
        
     

        
    }
    
    func show(hideTime hideTime: Double) {
        if visible { return }
        
        charNode.runAction(SKAction.moveByX(0, y: 80, duration: 1))
        visible = true
        isHit = false
        
       
            charNode.texture = SKTexture(imageNamed: "penguinGood")
            charNode.name = "charFriend"
            
        charNode.xScale = 1
        charNode.yScale = 1
        
        RunAfterDelay(hideTime * 3.5) { [unowned self] in
            self.hide()
        }
    }
    
    func hide() {
        if !visible { return }
        
        charNode.runAction(SKAction.moveByX(0, y:-80, duration:0.5))
        visible = false
    }
    
    func hit() {
        isHit = true
        
        let delay = SKAction.waitForDuration(0.25)
        let hide = SKAction.moveByX(0, y:-80, duration:0.5)
        let notVisible = SKAction.runBlock { [unowned self] in self.visible = false }
        charNode.runAction(SKAction.sequence([delay, hide, notVisible]))
    }
}
